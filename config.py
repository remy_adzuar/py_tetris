import pygame

class Config:

    def __init__(self) -> None:
        self.screen = pygame.display.set_mode((400, 500))
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font("freesansbold.ttf", 16)
        self.sounds = {}

        self.add_sounds("line_supp","./sounds/line_supp.wav")
        self.add_sounds("collide", "./sounds/collide.wav")
        self.add_sounds("fail", "./sounds/fail.wav")
    
    def get_screen(self) -> pygame.Surface:
        return self.screen
    
    def get_clock(self) -> pygame.time.Clock:
        return self.clock
    
    def get_font(self) -> pygame.font.Font:
        return self.font

    def add_sounds(self, name:str, path:str):
        self.sounds[name] = pygame.mixer.Sound(path)

    def play_sounds(self, name:str):
        if name in self.sounds.keys():
            pygame.mixer.Sound.play(self.sounds[name])