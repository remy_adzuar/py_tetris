import pygame

class Dimension:
    def __init__(self, topleft_x, topleft_y, width, height, color=(255,255,255)) -> None:
        self.x, self.y, self.width, self.height = topleft_x, topleft_y, width, height
        self.set_rect()
        self.color = color # WHITE DEFAULT COLOR

    def set_rect(self):
        self.rect = pygame.Rect(self.x,self.y,self.width,self.height)

    def get_rect(self):
        return self.rect

    def move(self, x:int, y:int, done=True):
        self.x += x
        self.y += y
        if done:
            self.set_rect()
    
    def set_pos(self, x,y,w,h):
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        self.set_rect()

    def update(self):
        self.set_rect()


class Entity:

    def __init__(self) -> None:
        self.last_move = None
        self.dimensions:list() = list()
        self.state = None
        self.fixed = False
    
    def get_state(self):
        return self.state

    def set_state(self, value):
        self.state = value


    def update(self):
        for dimension in self.dimensions:
            if isinstance(dimension, Dimension):
                # dimension.move(0,0)
                dimension.update()


    def draw(self, screen:pygame.Surface):
        for dimension in self.dimensions:
            if isinstance(dimension, Dimension):
                rect = dimension.get_rect()
                pygame.draw.rect(screen, dimension.color, rect)

    def process_input(self):
        pass

class Tetro(Entity):

    def __init__(self) -> None:
        super().__init__()
        self.inputable = True
        self.color = (255,255,255) # WHITE DEFAULT
        self.dimensions = [
            Dimension(0,0,15,15, self.color)
            # ,Dimension(16,0,15,15, self.color),Dimension(32,0,15,15,self.color),
            # Dimension(0,16,15,15,self.color)
        ]


    def move(self, x,y, done=True):
        # Si done est True le mouvement est effectue
        if not self.fixed:
            for dimension in self.dimensions:
                if isinstance(dimension, Dimension):
                    self.last_move = (x,y)
                    dimension.move(x,y, done)

    def update(self):
        for dimension in self.dimensions:
            if isinstance(dimension, Dimension):
                # dimension.update()
                pass
        if self.state == "SIDE_COLLIDE":
            self.undo_move()
            self.state = None
        if self.state == "BOTTOM_COLLIDE":
            self.undo_move()
            self.fixed = True
            self.state = None
        if self.state == "GRID_COLLIDE":
            self.undo_move()
            self.fixed = True
            self.state = None


    def undo_move(self):
        if self.last_move != None:
            last = self.last_move
            self.move(self.last_move[0]*-1, self.last_move[1]*-1)
            self.last_move = last
        # self.last_move = None    

    def process_input(self, key):
        if key == pygame.K_RIGHT:
            self.last_move = (16,0)
            self.move(16,0)
        if key == pygame.K_LEFT:
            self.last_move = (-16,0)
            self.move(-16,0)
        if key == pygame.K_DOWN:
            self.last_move = (0,16)
            self.move(0,16)

class R_tetro(Tetro):
    # Tetronomino capable de rotation
    def __init__(self) -> None:
        super().__init__()

    def process_input(self, key):
        super().process_input(key)
        if key == pygame.K_UP:
            self.rotate_90()

    def rotate_90(self, center_pos=1):
        center = self.dimensions[center_pos]
        list_pos = []
        for dimension in self.dimensions:
            list_pos.append([dimension.rect.x-center.rect.x, dimension.rect.y-center.rect.y])
        
        new_pos = []
        for pos in list_pos:
            new_pos.append([-pos[1],pos[0]])
        
        i = 0
        for dimension in self.dimensions:
            pos = new_pos[i]
            pos2 = list_pos[i]
            dimension.move(-pos2[0],-pos2[1])
            dimension.move(pos[0], pos[1])
            i += 1

class O_tetro(Tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (255,255,0)
        self.dimensions = [
            Dimension(0,0,15,15, self.color), Dimension(16,0,15,15, self.color),
            Dimension(0,16,15,15, self.color), Dimension(16,16,15,15, self.color),
        ]

class I_tetro(R_tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (0,255,255)
        self.dimensions = [
            Dimension(0,0,15,15, self.color), Dimension(16,0,15,15, self.color), Dimension(32,0,15,15,self.color), Dimension(48,0,15,15,self.color)
        ]

class L_tetro(R_tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (255,128,0)
        self.dimensions = [
            Dimension(0,0,15,15, self.color),Dimension(16,0,15,15, self.color),Dimension(32,0,15,15, self.color),
            Dimension(0,16,15,15, self.color)
        ]

class J_tetro(R_tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (0,255,0)
        self.dimensions = [
            Dimension(0,0,15,15,self.color),Dimension(16,0,15,15,self.color),Dimension(32,0,15,15,self.color),
            Dimension(32,16,15,15,self.color)
        ]

class T_tetro(R_tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (128,0,255)
        self.dimensions = [
            Dimension(0,0,15,15,self.color),Dimension(16,0,15,15,self.color),Dimension(32,0,15,15,self.color),
            Dimension(16,16,15,15,self.color)
        ]

class Z_tetro(R_tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (255,0,0)
        self.dimensions = [
            Dimension(0,0,15,15,self.color), Dimension(16,0,15,15,self.color),
            Dimension(16,16,15,15,self.color), Dimension(32,16,15,15, self.color)
        ]

class S_tetro(R_tetro):

    def __init__(self) -> None:
        super().__init__()
        self.color = (0,255,0)
        self.dimensions = [
            Dimension(16,0,15,15,self.color),Dimension(32,0,15,15,self.color),
            Dimension(0,16,15,15,self.color), Dimension(16,16,15,15,self.color)
        ]
    def rotate_90(self, center_pos=1):
        return super().rotate_90(0)
