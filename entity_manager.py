import pygame
import random

from config import Config
from entity import *
from grid import Grid
from text import Text


class Entity_Manager:

    def __init__(self, config:Config) -> None:
        self.config = config

        first = self.new_tetro()
        first.move(48,0)
        second = self.new_tetro()
        second.move(48,0)
        self.entities = [first, second]

        self.current = self.entities[0]
        self.next = self.entities[1]
        self.grid = Grid(self.config, self.current)

        self.lines_removed = 0
        self.speed = self.calc_speed()
        self.score = 0
    
    def add_entity(self, entity:Entity):
        self.entities.append(entity)
    
    def get_entity(self, pos):
        if pos < len(self.entities):
            return self.entities[pos]
    
    def get_speed(self):
        return self.speed
    
    def get_score(self) -> int:
        return self.score

    def score_update(self, nb):
        if nb == 1:
            self.score += 10 + (self.lines_removed//10)*10
        if nb == 2:
            self.score += 20 + 2 * (self.lines_removed//10)*10
        if nb == 3:
            self.score += 40 + 3 * (self.lines_removed//10)*10
        if nb == 4:
            self.score += 80 + 4 * (self.lines_removed//10)*10
        print(self.score)

    def calc_speed(self):
        if self.lines_removed < 49:
            calc_speed = 1000 - (150*(self.lines_removed//10))
        else:
            calc_speed = 500 - (50*(self.lines_removed//10))
        max_speed = 150
        speed = max(calc_speed, max_speed)
        return speed

    def update(self):
        self.grid.check_border()
        self.grid.check_bottom()
        self.grid.check_grid_collide()
        self.current.update()
        if self.current.fixed:
            ## apres fix to grid
            nb_lines = self.grid.fix_tetro()
            self.lines_removed += nb_lines
            self.score_update(nb_lines)
            
            self.speed = self.calc_speed()
            self.new_current()

        self.grid.check_first_line()
        print(self.grid.state)
        if self.grid.state == "DEAD":
            return "DEAD"
    
    def drop_current(self):
        self.current.move(0,16)
        self.grid.check_grid_collide()
    
    def draw(self):
        self.current.draw(self.config.get_screen())
        self.grid.draw()
        self.draw_next()
        self.draw_text()
    
    def draw_text(self):
        text_level = Text(self.config, (8,354), "LIGNES : "+str(self.lines_removed))
        text_score = Text(self.config, (8+text_level.rect.width+16,354), "SCORES : "+str(self.score))
        text_speed = Text(self.config, (8,374), "SPEED : "+str(self.speed))

        text_level.print_text()
        text_score.print_text()
        text_speed.print_text()
        
    def draw_next(self):
        next = self.next
        next.move(160,16)
        next.draw(self.config.get_screen())
        next.move(-160,-16)
        self.next.last_move = None

    def process_input(self, key:pygame.key):
        self.current.process_input(key)
        if key == pygame.K_SPACE:
            loop = True
            while loop:
                self.current.last_move = (0,16)
                self.current.move(0,16)
                self.grid.check_bottom()
                self.grid.check_grid_collide()
                self.current.update()
                res = self.grid.check_first_line()
                if res == "DEAD":
                    return "DEAD"
                if self.current.fixed:
                    loop = False
                    pass

        # self.grid.check_current()
        self.grid.check_border()
        self.grid.check_bottom()


    ##

    ## FONCTION UTILITAIRES

    ##
    
    ##

    def new_current(self):

        self.config.play_sounds("collide")
        self.entities.pop(0)
        new = self.new_tetro()
        new.move(48,0)
        self.entities.append(new)
        self.current = self.entities[0]
        self.next = self.entities[1]
        self.grid.current = self.current
    
    def new_tetro(self):

        i = random.randrange(7)
        if i == 0:
            return I_tetro()
        if i == 1:
            return O_tetro()
        if i == 2:
            return T_tetro()
        if i == 3:
            return L_tetro()
        if i == 4:
            return J_tetro()
        if i == 5:
            return Z_tetro()
        if i == 6:
            return S_tetro()