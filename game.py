import pygame

from config import Config
from entity_manager import Entity_Manager

class Game:

    def __init__(self, config:Config) -> None:
        self.config = config
        
        return None
    
    def load(self):
        self.EM = Entity_Manager(self.config)
        self.start = pygame.time.get_ticks()

    def update(self):

        speed = self.EM.get_speed()
        if (pygame.time.get_ticks()-self.start)/speed > 1:
            self.start = pygame.time.get_ticks()
            self.EM.drop_current()
        res = self.EM.update()
        if res == "DEAD":
            return False
        return True

    def draw(self):
        self.config.get_screen().fill((0,0,0))
        
        self.EM.draw()

        pygame.display.update()
    
    def process_input(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.loop = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    self.EM.process_input(event.key)
                if event.key == pygame.K_RIGHT:
                    self.EM.process_input(event.key)
                if event.key == pygame.K_DOWN:
                    self.EM.process_input(event.key)
                if event.key == pygame.K_UP:
                    self.EM.process_input(event.key)
                if event.key == pygame.K_SPACE:
                    self.EM.process_input(event.key)