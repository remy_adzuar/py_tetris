import pygame

from entity import *
from config import Config

class Grid:

    def __init__(self, config:Config, current) -> None:
        self.config = config
        self.current = current
        self.width = 10
        self.height = 22
        self.size = (16,16)
        self.state = "ALIVE"
        self.dimension = Dimension(0,0,self.size[0]*(self.width), self.size[1]*(self.height))
        self.matrix = [[None for i in range(self.width)] for j in range(self.height)]

    def get_case(self,x:int,y:int):
        # x, et y sont des position de case
        if x < self.width and y < self.height:
            return self.matrix[y][x] # Attention x, y paraissent inverse mais c'est bien dans ce sens la
        else:
            return None
    
    def get_pixel_case(self, x:int, y:int):
        # x, y sont des position de pixel
        if y >= 0:
            case_x = x//self.size[0]
            case_y = y//self.size[1]
            if case_x < self.width and case_x>=0 and case_y < self.height:
                return self.matrix[case_y][case_x]
            else:
                if case_x >= self.width or case_x < 0:
                    return "SIDE"
                if case_y > self.height:
                    return "BOTTOM"
        return None
    
    def calc_pixel_coords(self, x:int, y:int):
        if y >= 0:
            case_x = x//self.size[0]
            case_y = y//self.size[1]
            if case_x < self.width and case_x>= 0 and case_y < self.height:
                return [case_x,case_y]
        return None

    def set_case(self, x:int, y:int, value):
        if x < self.width and y < self.height:
            if value == None:
                self.matrix[y][x] = value
            elif isinstance(value, Dimension):
                self.matrix[y][x] = value
            elif isinstance(value, int):
                self.matrix[y][x] = value
    
    def set_pixel_case(self, x:int, y:int, value):
        if x >= 0 and y >= 0:
            case_x = x//self.size[0]
            case_y = y//self.size[1]
            if case_x < self.width and case_y < self.height:
                if value == None:
                    self.matrix[case_y][case_x] = value
                elif isinstance(value, Dimension):
                    self.matrix[case_y][case_x] = value

    def check_lines(self):
        y = 0
        cpt = 0
        while y < self.height:
            if not None in self.matrix[y]:
                self.remove_line(y)
                cpt += 1
            y+=1
        if cpt>0:
            self.config.play_sounds("line_supp")
        return cpt
    
    def remove_line(self, pos):
        # Si la ligne est remplie on la retire et on en rajoute une au debut de la liste
        new_line = [None for i in range(self.width)]
        if pos < self.height:
            self.matrix.pop(pos)
            self.matrix.insert(0, new_line)
            return True
        return False

    def get_line(self, pos):
        if pos < self.height and pos >= 0:
            return self.matrix[pos]
        return "ERROR LINE"
    
    def fix_tetro(self):
        # L'entite a touche le bottom de la grille, et doit donc etre fixee sur la grille
        for dimension in self.current.dimensions:
            if isinstance(dimension, Dimension):
                case_grid = self.calc_pixel_coords(dimension.rect.centerx, dimension.rect.centery)
                # print(case_grid)
                if case_grid != None:
                    self.set_case(case_grid[0], case_grid[1], dimension)
        
        lines_removed = self.check_lines()
        return lines_removed
    
    def check_current(self):
        for dimension in self.current.dimensions:
            if isinstance(dimension, Dimension):
                rect = dimension.get_rect()
                if rect.x < 0 or rect.x+rect.width > self.width*self.size[0]:
                    # print("BORDURE COLLISION")
                    # self.current.set_state("SIDE_COLLIDE")
                    self.current.undo_move()
        return False
    
    def check_border(self):
        for dim in self.current.dimensions:
            if isinstance(dim, Dimension):
                rect = dim.get_rect()
                if rect.x < 0:
                    self.current.move(16,0)
                elif rect.x+rect.width> self.width*self.size[0]:
                    self.current.move(-16,0)
                
    def check_bottom(self):
        # Recursivite mon amour
        bottom_touch = False
        for dim in self.current.dimensions:
            if isinstance(dim, Dimension):
                rect = dim.get_rect()
                bottom = rect.y+rect.height
                if bottom> self.height*self.size[1]+16:
                    ## On depasse beaucoup, possible avec une rotation par exemple
                    self.current.move(0,-16)
                    self.check_bottom()
                if bottom > self.height*self.size[1] and bottom < self.height*self.size[1]+16:
                    ## On ne depasse plus de beaucoup
                    self.current.move(0,-16)
                    bottom_touch = True
        if bottom_touch:
            self.current.fixed = True
    
    def check_grid_collide(self):
        # Verif si colision avec des Dimension deja dans la grid
        for dimension in self.current.dimensions:
            if isinstance(dimension, Dimension):
                coords = self.calc_pixel_coords(dimension.rect.centerx, dimension.rect.centery)
                if coords != None:
                    case_grid = self.get_case(coords[0],coords[1])
                    if case_grid != None:
                        if isinstance(case_grid, Dimension):
                            if self.current.last_move != None:
                                if self.current.last_move[1]>0:
                                    self.current.set_state("GRID_COLLIDE")
                                else:
                                    self.current.undo_move()
                                return "GRID_COLLIDE"

    def check_first_line(self):
        ## Si il y a des blocs fixes dans cette ligne la partie est perdu
        line = self.matrix[0]
        for elem in line:
            if elem != None:
                self.state = "DEAD"

    def draw(self):
        pygame.draw.rect(self.config.get_screen(), (255,255,255), self.dimension.get_rect(),1)
        y = 0
        while y < self.height:
            x = 0
            while x < self.width:
                c = self.get_case(x,y)
                if c != None and isinstance(c, Dimension):
                    # print(c)
                    c.set_pos(x*16,y*16,15,15)
                    pygame.draw.rect(self.config.get_screen(), (255,255,255), c.get_rect())
                x += 1
            y += 1

if __name__ == "__main__":

    G = Grid(Config(),O_tetro())
    print(G.matrix)

    G.set_case(0,0, Dimension(1,1,1,1))
    print(G.get_line(0))
    G.remove_line(0)
    print(G.get_line(0))

    G.set_case(1,1, 5)
    print(G.get_pixel_case(16,336))