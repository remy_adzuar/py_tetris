import pygame

from config import Config
from entity import *
from state import State

class Main:

    def __init__(self) -> None:

        self.config = Config()
        self.state = State(self.config)

        self.loop = True
        while self.loop:

            if self.state.get_state() == "MENU":
                self.state.menu()
            
            elif self.state.get_state() == "NEWGAME":
                score = self.state.game()
                self.config.play_sounds("fail")
                self.state.scores(True, score)

            elif self.state.get_state() == "SCORE":
                self.state.scores()
        
            elif self.state.get_state() == "CREDITS":
                self.state.credits()

            elif self.state.get_state() == "QUIT":
                self.loop = False
                pygame.quit()

        exit()

if __name__ == "__main__":

    pygame.init()

    M = Main()