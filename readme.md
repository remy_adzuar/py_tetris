# Projet Tetris

Implementer un tetris avec python3 et pygame

## Fonctionalites :

Des tetronomino tombent
Le joueur peut retourner et positionner lateralement les tetronomino
Si une ligne est constituee elle disparait
Si un bloc depasse en haut la partie est perdue

## Prevision :

10 min, preparation papier
10 min, preparation PC

16 x 25 min pour coder un prototype fonctionnel
16 x 25 min pour peaufiner le prototype

## Idees :

Enregistrer le score du joueur
Accelerer le jeu petit a petit
Pouvoir prevoir la position du tetronomino (mode facile)
Code couleur pour les tetronomino
Indiquer le tetronomino suivant
Ajouter des effet en overlay et des sons

## Suivis projet

1 ere session
25 min, Base du projet, architecture avec entity manager, une config, et Dimension/Entity
Fin session

2 eme session
25 min, Une entite contient une liste de dimension, Debut classe grid
5 min pause,
25 min, test implementation des input via l'entity manager, classe grid en cours
5 min pause,
25 min, Test implementation colision avec base de grille
5 min pause,
25 min, Ajout colision avec bordure de la grid en cours d'implementation
Fin de session

3eme session
25 min, Blocage sur la grille et les collisions
Fin de session

5 min pause,
25 min, Refactorisation Entity, Changement Systeme d'update
5 min pause,
25 min, Implementation des colisions avec bordures en cours
Fin de session reste 8 x 25 min

25/12/2022
4 eme session
25 min, Implementation des colisions
5 min pause,
25 min, Colision bottom et side ok, suppression de ligne ok
5 min pause,
25 min, Colision entre current et grid ok
Fin de session

5 eme session
25 min, Premiers test de rotation
5 min pause,
25 min, Peaufinage rotation, pas mal de bug
5 min pause,
25 min, Amelioration Entity et de leur classe
5 min pause,
25 min, Ajout derniers tetronominos, Bug aleatoire apres quelques chute et ligne effacees
5 min pause,
25 min, resolution bug, il ne faut pas reutiliser les adresses memoire des tetronomino, sinon ils sont deja en bas...
Fin de session Fin de prototype

## Observations

Le developpement a ete plus hardu que prevu, j'ai meme fais appel a GPT3 pour maider a coder une rotation propre et efficace, j'avais deja trouve cet algo
mais impossible de m'en souvenir. Bref beaucoup de bug difficile a resoudre, pour le dernier remonter a la source du bug a ete tres difficile mais il 
m'arrive souvent d'avoir des soucis avec les adresses memoire donc des que les soupcons se sont portes sur la generation de tetronomino, resoudre le bug a
ete evident et simple
Le prototype est jouable, mais il reste beaucoup de chemin pour polisser le jeu

26/12/2022

6 eme session
25 min, Ajout condition de defaite, debut du fast down,
5 min pause,
25 min, Fast down ok, afficher la piece suivante ok, augmentation de la vitesse ok
5 min pause,
25 min, Faire tomber les piece du centre, ajout du score
5 min pause,
25 min, ajout classe Text et affichage du score, du nombre de ligne et de la vitesse du jeu
Fin de session

7 eme session
25 min, Debut ajout du tableau de score
5 min pause,
25 min, Poursuite Ecran Entree du nom ok
5 min pause,
25 min, Poursuite implementation des ecrans (menu, jeu etc)
5 min pause,
25 min, Creation de la classe game, et deplacement de la logique du jeu

8 eme session
25 min, Ajout des text clickable avec une classe dediee
5 min pause,
25 min, Debut ajout credits et probleme de persistance du click
5 min pause,
25 min, Ajout du score, une fois la partie perdue
5 min pause,
25 min, Ajout des sons

fin de session

Fin de projet

## Observation

Reste 4x25min de budget temps.
Le systeme de state et de texte clickable commence a etre satisfaisant. L'ajout d'une grid permet de rendre plus facile les colision. Construire des entites plus complexes qui contiennent une liste de dimension n'a pas ete une mince affaire mais je ne vois pas d'autre solution plus simples...

Au global le code me parait mieux structure que dans le precedent projet de casse brique