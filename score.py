import pygame

from config import Config
from text import Text

class Score:
    """
    Classe qui va gerer l'ecran de score, l'ecriture et la sauvegarde des scores sur un fichier externe
    """

    def __init__(self, config:Config, new_score=False, score=None) -> None:
        self.config = config
        self.file = "./score.txt"
        self.read_score_file()
        self.sort_score()
        

        self.new_score = new_score
        if self.new_score:
            self.name = ""
            self.score_player = score

        self.state = "ALIVE"
        
        
        self.loop = True
        while self.loop:
            self.draw_score_screen()
            self.process_key()
            if self.state == "DEAD":
                self.loop = False

    def read_score_file(self):
        file = open(self.file, "r")
        self.score = []
        for line in file:
            line_strip = line.strip()
            line_split = line_strip.split(",")
            self.score.append([line_split[0],int(line_split[1])])
        file.close()

    def write_score_file(self):
        file = open(self.file, "w")
        self.sort_score()
        for line in self.score:
            if len(line) == 2:
                s = str(line[0])+","+str(line[1])+"\n"
                file.write(s)
        file.close()

    def sort_score(self):
        self.score.sort(key= lambda x:x[1], reverse=True)

    def get_score_min(self):
        self.sort_score()
        return self.score[len(self.score)-1]

    def can_append(self, score):
        # retourne vrai si le score peut etre ajoute, faux sinon
        if len(score) == 2:
            try:
                str(score[0])
                int(score[1])
            except:
                print("ERREUR DANS SCORE")
            if len(self.score) < 10:
                return True
            else:
                score_min = self.get_score_min()
                if int(score_min[1]) < int(score[1]):
                    return True
        return False

    def append_score(self, score):
        can_append = self.can_append(score)
        if can_append:
            if len(self.score) < 10:
                self.score.append(score)
            else:
                self.score[-1] = score
            self.sort_score()

    def score_screen(self):
        pass

    def print_console(self):
        for line in self.score:
            print(line)

    def process_key(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.loop = False
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                if self.new_score:
                    if event.key == pygame.K_BACKSPACE:
                        self.name = self.name[:-1]
                    elif event.key == pygame.K_RETURN:
                        if self.name.isalnum() and len(self.name)>2:
                            self.append_score([self.name.upper(),self.score_player])
                            self.write_score_file()
                            self.new_score = False
                    elif event.unicode.strip() != "" and len(self.name) <= 12: 
                        self.name += event.unicode.strip()
                
                else:
                    # Soit on consulte simplement le tableau de score, soit on vient d'entrer son score
                    if event.key != None:
                        self.loop = False


    def draw_score_screen(self):
        self.config.get_screen().fill((0,0,0))
        text1 = Text(self.config, (8,16), "ECRAN DE SCORE")
        text1.print_text()
        if self.new_score:
            text_name = Text(self.config, (8,32), "ENTREZ NOM : "+str(self.name))
            text_name.print_text()

        text2 = Text(self.config, (8, 64), "TOP 10")
        text2.print_text()
        y = 0
        for score in self.score:
            score_text = Text(self.config, (8, 64+(y*24)), str(y)+" | "+score[0]+" : "+str(score[1]))
            score_text.print_text()
            y += 1
        
        if not self.new_score:
            text_fin1 = Text(self.config, (8,64+(11*24)), "Appuyez sur une touche...")
            text_fin2 = Text(self.config, (8,64+(12*24)), "pour revenir au menu principal")
            text_fin1.print_text()
            text_fin2.print_text()

        pygame.display.update()

if __name__ == "__main__":

    pygame.init()
    C = Config()
    S = Score(C, True, 500)
    # S.file = "./scoreTest.txt"
    # S.append_score(["JESS",460])
    S.print_console()

    # S.write_score_file()
    # print(S.get_score_min())