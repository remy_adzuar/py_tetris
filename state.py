import pygame

from config import Config
from text import Text, Select_Text
from game import Game
from score import Score

class State:

    def __init__(self, config:Config) -> None:
        self.config = config
        self.state = "MENU"
        self.loop = False
    
    def get_state(self):
        return self.state

    def menu(self):
        text1 = Select_Text(self.config, "NEWGAME", (8,16), "1. Nouvelle partie")
        text2 = Select_Text(self.config, "SCORE", (8,32), "2. Tableau Score")
        text3 = Select_Text(self.config, "CREDITS", (8,48), "3. Credits")
        text4 = Select_Text(self.config, "QUIT" ,(8,64), "4. Quitter")
        texts = [text1,text2,text3,text4]

        self.loop = True
        while self.loop:

            self.config.get_screen().fill((0,0,0))

            for text in texts:
                text.print_text()

            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.loop = False
                    self.state = "QUIT"

                if event.type == pygame.MOUSEBUTTONDOWN:
                    cursor_click = pygame.mouse.get_pressed()
                    print(cursor_click)
                    if cursor_click[0]:
                        for text in texts:
                            if text.rect.collidepoint(pygame.mouse.get_pos()):
                                self.loop = False
                                self.state = text.get_target()
                    cursor_click = None
    
    def game(self):
        G = Game(self.config)

        self.loop = True
        G.load()
        while self.loop:
            loop = G.update()
            G.draw()
            G.process_input()

            if not loop:
                self.loop = False

            self.config.get_clock().tick(60)

        score = G.EM.score
        self.state = "MENU"
        return score
    
    def credits(self):
        self.click_relapse = False
        texts = [
            Text(self.config, (8,8), "CREDITS : "),
            Text(self.config, (16,40), "Developpeur : ADZUAR REMY"),
            Text(self.config, (16,62), "STUDIO : LOW GRADE GAMES")
        ]

        retour_menu = Select_Text(self.config, "MENU", (16,128), "Retour MENU")

        self.loop = True

        while self.loop:
            self.config.get_screen().fill((0,0,0))

            for text in texts:
                text.print_text()
            retour_menu.print_text()
            
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    print(event.type)


                if pygame.mouse.get_pressed()[0]:
                    self.loop = False
                    if retour_menu.rect.collidepoint(pygame.mouse.get_pos()):
                        self.state = retour_menu.get_target()
        self.state = "MENU"

    def scores(self, new_score=False, score=None):
        
        S = Score(self.config, new_score, score)

        self.state = "MENU"