import pygame

from config import Config

class Text:

    def __init__(self, config:Config, pos, content:str="", center:bool=False) -> None:
        self.config = config
        self.pos = pos
        self.content = content
        self.center = center

        self.tx_color = (255,255,255)
        self.bg_color = (50,50,50)
        self.load_text()


    def set_tx_color(self, color):
        self.tx_color = color

    def set_bg_color(self, color):
        self.bg_color = color

    def load_text(self):
        font = self.config.get_font()
        rendu = font.render(self.content, True, self.tx_color, self.bg_color)
        self.rect = rendu.get_rect()
        if self.center:
            self.rect.center = self.pos
        else:
            self.rect.topleft = self.pos
        self.rendu = rendu

    def print_text(self):
        self.load_text()
        screen = self.config.get_screen()
        screen.blit(self.rendu, self.rect)

class Select_Text(Text):

    def __init__(self, config: Config, target, pos, content: str = "", center: bool = False) -> None:
        super().__init__(config, pos, content, center)
        self.highlight = False
        self.target = target
    
    def print_text(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.rect.collidepoint(mouse_pos):
            self.prec_bg_color = self.bg_color
            self.bg_color = (255,255,255)
            self.prec_tx_color = self.tx_color
            self.tx_color = (255,0,0)
            self.highlight = True
        else:
            if self.highlight == True:
                self.bg_color = (50,50,50)
                self.tx_color = (255,255,255)
                self.highlight = False

        super().print_text()
    
    def get_target(self):
        return self.target
        
